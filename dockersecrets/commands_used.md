# Commands used in this setup

## Most important commands

Start Container by running: *docker-compose.dockersecrets.up.bat*

```shell
ssh-keygen -t rsa -b 4096 -C your@email.no -P MyPassword -f /templateworkspace_secrets/ubuntu_rsa
```